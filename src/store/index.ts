import Vue from 'vue'
import Vuex from 'vuex'

import list from '@/views/List/store'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    list
  },
  strict: debug,
  plugins: debug ? [] : []
})
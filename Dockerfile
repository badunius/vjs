# build
FROM node:10-alpine as build


ARG APP_DIR=app
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
ENV PATH ${APP_DIR}/node_modules/.bin:$PATH

COPY package*.json ./
RUN npm install --silent
RUN npm install @vue/cli@3.7.0 -g

COPY . .
RUN npm run build

# serve
FROM nginx:1.16.0-alpine
COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
var testServer = 'http://localhost:8000';

module.exports = {
	lintOnSave: false,
	devServer: {
		port: 3500,
		proxy: {
			'/api': {
				target: testServer,
				changeOrigin: true,
			},
			'/static': {
				target: testServer,
				changeOrigin: true,
			},
		},
	},
};

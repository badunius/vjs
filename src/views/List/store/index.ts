import * as api from '../api'
import to from 'await-to-js'

interface listItem {
  id: number,
  label: string,
}

interface listState {
  items: listItem[],
  loading: boolean,
}

const state: listState = {
  items: [],
  loading: false,
}

const getters = {
  isLoading: (state) => state.loading
}

const actions = {
  async fetchItems({commit}, payload) {
    const {
      offset = 0, 
      count = 10
    } = payload || {}
    
    commit('setLoadingState', true)
    const [err, res] = await to(api.getItemList(offset, count))
    
    if (!err) {
      const { data } = <{data:any}>res
      commit('setListItems', data)
    }
    commit('setLoadingState', false)
  },
}

const mutations = {
  setListItems(state, newItems: listItem[]) {
    state.items = [...newItems]
  },

  setLoadingState(state, loading) {
    state.loading = loading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
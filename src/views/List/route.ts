import ListMain from './page/ListMain.vue'
import ListItemView from './page/ListItemView.vue'

const routes = [
  {
    path: '/list',
    name: 'list',
    component: ListMain,
  },
  {
    path: '/list/:id',
    name: 'list-item',
    component: ListItemView
  }
]

export default routes
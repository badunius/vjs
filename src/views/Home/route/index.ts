import HomeMain from '../page/HomeMain.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeMain,
  }
]

export default routes
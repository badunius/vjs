import Vue from 'vue'
import App from '@/app/App.vue'
import router from './router'
import store from './store'
import './styles/global.scss'

Vue.config.productionTip = false

const app = new Vue({
	el: '#app',
	store,
	router,
	render: h => h(App),
	components: { App },
})

export default app

module.exports = {
  presets: [
    '@vue/app'
  ],
  plugins: [
    "@babel/plugin-proposal-nullish-coalescing-operator",
    "@babel/plugin-syntax-optional-chaining",
    "@babel/plugin-proposal-object-rest-spread"
	]
}

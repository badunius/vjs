export function getItemList(offset = 0, count = 20) {
  
  const data = (new Array(count)).fill(0).map((el, idx) => ({
    id: idx + offset,
    label: `item ${idx + offset}`
  }))

  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({ data, status: 200 });
    }, Math.random() * 500);
  })
}
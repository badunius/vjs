import Vue from 'vue';
import VueRouter from 'vue-router'
import Home from '@/views/Home/route'
import List from '@/views/List/route'

Vue.use(VueRouter);

const routes = [
  ...Home,
  ...List
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
